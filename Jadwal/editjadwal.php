<?php
include 'db.php';

$data = mysqli_query($conn, "SELECT * FROM tbjadwal WHERE id_Jadwal = '" . $_GET['id'] . "'");
$r = mysqli_fetch_array($data);

$id_dosen = $r['id_dosen'];
$id_kelas = $r['id_kelas'];
$jadwal = $r['jadwal'];
$matakuliah = $r['matakuliah'];
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">


    <title>Hello, world!</title>

    <style type="text/css">
        * {
            font-family: "Trebuchet MS";
        }

        h1 {
            text-transform: uppercase;
            color: blue;
        }

        button {
            background-color: blue;
            color: #fff;
            padding: 10px;
            text-decoration: none;
            font-size: 12px;
            border: 0px;
            margin-top: 20px;
        }

        label {
            margin-top: 10px;
            float: left;
            text-align: left;
            width: 100%;
        }

        input {
            padding: 6px;
            width: 100%;
            box-sizing: border-box;
            background: #f8f8f8;
            border: 2px solid #ccc;
            outline-color: blue;
        }

        div {
            width: 100%;
            height: auto;
        }

        .base {
            width: 400px;
            height: auto;
            padding: 20px;
            margin-left: auto;
            margin-right: auto;
            background: #ededed;
        }
    </style>

</head>

<body>

    <center>
        <h1>Tambah Data</h1>
        <center>
            <form method="POST" action="" enctype="multipart/form-data">
                <section class="base">
                    <div>
                        <label>id_dosen</label>
                        <input type="number" name="id_dosen" value="<?php echo $id_dosen ?>" autofocus="" required="" />
                    </div>
                    <div>
                        <label>id_kelas</label>
                        <input type="number" name="id_kelas" value="<?php echo $id_kelas ?>" autofocus="" required="" />
                    </div>
                    <div>
                        <label>jadwal</label>
                        <input type="date" name="jadwal" value="<?php echo $jadwal ?>" required="" />
                    </div>
                    <div>
                        <label>matakuliah</label>
                        <input type="text" name="matakuliah" value="<?php echo $matakuliah ?>" required="" />
                    </div>
                    <div>
                        <button type="submit" name="kirim">Simpan Data</button>
                    </div>
                    <br>
                    <div class="col-4">
                        <a class="btn btn-outline-primary " href="datajadwal.php" role="button">Data</a>
                    </div>
                </section>
            </form>

            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

            <?php

            if (isset($_POST['kirim'])) {
                $id_dosen = $_POST['id_dosen'];
                $id_kelas = $_POST['id_kelas'];
                $jadwal = $_POST['jadwal'];
                $matakuliah = $_POST['matakuliah'];


                if ($jadwal != '') {
                    $update = mysqli_query($conn, "UPDATE tbjadwal SET 
           id_dosen = '" . $id_dosen . "',
          id_kelas = '" . $id_kelas . "',
          jadwal = '" . $jadwal . "',
          matakuliah = '" . $matakuliah . "'
           
           WHERE id_Jadwal = '" . $_GET['id'] . "'
           ");
                    if ($update) {
                        echo 'Berhasil Upadate';
                    } else {
                        echo 'Gagal Upadate';
                    }
                }
            }

            ?>

</body>

</html>