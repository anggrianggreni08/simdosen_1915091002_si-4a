<?php
// memanggil file koneksi.php untuk membuat koneksi
include '../koneksi.php';

// mengecek apakah di url ada nilai GET id_kelas
if (isset($_GET['id_kelas'])) {
    // ambil nilai id_kelas dari url dan disimpan dalam variabel $id_kelas
    $id_kelas = ($_GET["id_kelas"]);

    // menampilkan data dari database yang mempunyai id_kelas=$id_kelas
    $query = "SELECT * FROM kelas WHERE id_kelas='$id_kelas'";
    $result = mysqli_query($koneksi, $query);
    // jika data gagal diambil maka akan tampil error berikut
    if (!$result) {
        die("Query Error: " . mysqli_errno($koneksi) .
            " - " . mysqli_error($koneksi));
    }
    // mengambil data dari database
    $data = mysqli_fetch_assoc($result);
    // apabila data tidak ada pada database maka akan dijalankan perintah ini
    if (!count($data)) {
        echo "<script>alert('Data tidak ditemukan pada database');window.location='index.php';</script>";
    }
} else {
    // apabila tidak ada data GET id_kelas pada akan di redirect ke index.php
    echo "<script>alert('Masukkan data id_kelas.');window.location='index.php';</script>";
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>CRUD KELAS</title>
    <style type="text/css">
    * {
        font-family: "Trebuchet MS";
    }

    h1 {
        text-transform: uppercase;
        color: blue;
    }

    button {
        background-color: blue;
        color: #fff;
        padding: 10px;
        text-decoration: none;
        font-size: 12px;
        border: 0px;
        margin-top: 20px;
    }

    label {
        margin-top: 10px;
        float: left;
        text-align: left;
        width: 100%;
    }

    input {
        padding: 6px;
        width: 100%;
        box-sizing: border-box;
        background: #f8f8f8;
        border: 2px solid #ccc;
        outline-color: blue;
    }

    div {
        width: 100%;
        height: auto;
    }

    .base {
        width: 400px;
        height: auto;
        padding: 20px;
        margin-left: auto;
        margin-right: auto;
        background: #ededed;
    }
    </style>
</head>

<body>
    <center>
        <h1>Edit Data kelas <?php echo $data['nama_kelas']; ?></h1>
        <center>
            <form method="POST" action="proses_edit.php" enctype="multipart/form-data">
                <section class="base">
                    <!-- menampung nilai id_kelas produk yang akan di edit -->
                    <input name="id_kelas" value="<?php echo $data['id_kelas']; ?>" hidden />
                    <div>
                        <label>Nama Kelas</label>
                        <input type="text" name="nama_kelas" value="<?php echo $data['nama_kelas']; ?>" autofocus=""
                            required="" />
                    </div>
                    <div>
                        <label>Prodi</label>
                        <input type="text" name="prodi" required="" value="<?php echo $data['prodi']; ?>" />
                    </div>
                    <div>
                        <label>Fakultas</label>
                        <input type="text" name="fakultas" required="" value="<?php echo $data['fakultas']; ?>" />
                    </div>
                    <div>
                        <button type="submit">Simpan Perubahan</button>
                    </div>
                </section>
            </form>
</body>

</html>