<?php
include '../koneksi.php' 
?>
<!DOCTYPE html>
<html>

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <title>CRUD KELAS</title>
    
</head>

<body>
<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <center>
        <h1>Data Hasil</h1>
        <center>
                <center>
                    <br />
                    <div class="container">
                    <table class="table-bordered border-primary table">
                        <thead>
                            <tr>
                                <th>id_kelas</th>
                                <th>Nama Kelas</th>
                                <th>Prodi</th>
                                <th>Fakultas</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $query = "SELECT * FROM kelas ORDER BY id_kelas ASC";
                            $result = mysqli_query($koneksi, $query);
                            if (!$result) {
                                die("Query Error: " . mysqli_errno($koneksi) .
                                    " - " . mysqli_error($koneksi));
                            }

                            $id_kelas = 1; 
                            while ($row = mysqli_fetch_assoc($result)) {
                            ?>
                            <tr>
                                <td><?php echo $id_kelas; ?></td>
                                <td><?php echo $row['nama_kelas']; ?></td>
                                <td><?php echo $row['prodi']; ?></td>
                                <td><?php echo $row['fakultas']; ?></td>
                                  <td>  <a href="edit_kelas.php?id_kelas=<?php echo $row['id_kelas']; ?>"><button type="button" class="btn btn-primary">Edit</button></a> |
                                    <a href="proses_hapus.php?id_kelas=<?php echo $row['id_kelas']; ?>"
                                        onclick="return confirm('Anda yakin akan menghapus data ini?')"><button type="button" class="btn btn-danger">Hapus</button></a></td>
                                </td>
                            </tr>

                            <?php
                                $id_kelas++; 
                            }
                            ?>
                        </tbody>
                    </table>
                    <a href="tambah_kelas.php"><button type="button" class="btn btn-info">Tambah Data</button></a>
</body>

</html>