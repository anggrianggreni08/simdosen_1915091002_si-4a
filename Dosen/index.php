<?php

include '../koneksi.php'

?>
<!DOCTYPE html>
<html>

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <title>CRUD DOSEN</title>
    
</head>

<body>
 <!-- Option 1: Bootstrap Bundle with Popper -->
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <center>
        <h1>Data Hasil</h1>
        <center>
                <center>
                    <br />
                    <div class="container">
                    <table class="table-bordered border-primary table">
                        <thead>
                            <tr>
                                <th>id_dosen</th>
                                <th>Nama</th>
                                <th>NIP</th>
                                <th>Prodi</th>
                                <th>Fakultas</th>
                                <th>Foto</th>
                                <th>action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $query = "SELECT * FROM dosen ORDER BY id_dosen ASC";
                            $result = mysqli_query($koneksi, $query);
                            if (!$result) {
                                die("Query Error: " . mysqli_errno($koneksi) .
                                    " - " . mysqli_error($koneksi));
                            }

                            $id_dosen = 1; 
                            while ($row = mysqli_fetch_assoc($result)) {
                            ?>
                            <tr>
                                <td><?php echo $id_dosen; ?></td>
                                <td><?php echo $row['nama_dosen']; ?></td>
                                <td><?php echo $row['nip_dosen']; ?></td>
                                <td><?php echo $row['prodi']; ?></td>
                                <td><?php echo $row['fakultas']; ?></td>
                                <td style="text-align: center;"><img src="gambar/<?php echo $row['foto_dosen']; ?>"
                                        style="width: 120px;"></td>
                                <td>
                                    <a href="edit_dosen.php?id_dosen=<?php echo $row['id_dosen']; ?>"><button type="button" class="btn btn-primary">Edit</button></a> |
                                    <a href="proses_hapus.php?id_dosen=<?php echo $row['id_dosen']; ?>"
                                        onclick="return confirm('Anda yakin akan menghapus data ini?')"><button type="button" class="btn btn-danger">Hapus</button></a>
                                </td>
                            </tr>

                            <?php
                                $id_dosen++; 
                            }
                            ?>
                        </tbody>
                    </table>
                    <a href="tambah_dosen.php"><button type="button" class="btn btn-info">Tambah Data</button></a>
                    </div>
</body>

</html>