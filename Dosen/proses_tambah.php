<?php
include '../koneksi.php';

$nama_dosen   = $_POST['nama_dosen'];
$nip_dosen     = $_POST['nip_dosen'];
$prodi    = $_POST['prodi'];
$fakultas    = $_POST['fakultas'];
$foto_dosen = $_FILES['foto_dosen']['name'];


if ($foto_dosen != "") {
    $ekstensi_diperbolehkan = array('png', 'jpg'); 
    $x = explode('.', $foto_dosen);
    $ekstensi = strtolower(end($x));
    $file_tmp = $_FILES['foto_dosen']['tmp_name'];
    $angka_acak     = rand(1, 999);
    $nama_gambar_baru = $angka_acak . '-' . $foto_dosen; 
    if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
        move_uploaded_file($file_tmp, 'gambar/' . $nama_gambar_baru); 
        $query = "INSERT INTO dosen (nama_dosen, nip_dosen, prodi, fakultas, foto_dosen) VALUES ('$nama_dosen', '$nip_dosen', '$prodi', '$fakultas', '$nama_gambar_baru')";
        $result = mysqli_query($koneksi, $query);
        if (!$result) {
            die("Query gagal dijalankan: " . mysqli_errno($koneksi) .
                " - " . mysqli_error($koneksi));
        } else {
            echo "<script>alert('Data berhasil ditambah.');window.location='index.php';</script>";
        }
    } else {
        echo "<script>alert('Ekstensi gambar yang boleh hanya jpg atau png.');window.location='tambah_dosen.php';</script>";
    }
} else {
    $query = "INSERT INTO dosen (nama_dosen, nip_dosen, prodi, fakultas, foto_dosen) VALUES ('$nama_dosen', '$nip_dosen', '$prodi', '$fakultas', null)";
    $result = mysqli_query($koneksi, $query);
    if (!$result) {
        die("Query gagal dijalankan: " . mysqli_errno($koneksi) .
            " - " . mysqli_error($koneksi));
    } else {
        echo "<script>alert('Data berhasil ditambah.');window.location='index.php';</script>";
    }
}