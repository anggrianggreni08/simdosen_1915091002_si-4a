<?php
// memanggil file koneksi.php untuk membuat koneksi
include '../koneksi.php';

// mengecek apakah di url ada nilai GET id_dosen
if (isset($_GET['id_dosen'])) {
    // ambil nilai id_dosen dari url dan disimpan dalam variabel $id_dosen
    $id_dosen = ($_GET["id_dosen"]);

    // menampilkan data dari database yang mempunyai id_dosen=$id_dosen
    $query = "SELECT * FROM dosen WHERE id_dosen='$id_dosen'";
    $result = mysqli_query($koneksi, $query);
    // jika data gagal diambil maka akan tampil error berikut
    if (!$result) {
        die("Query Error: " . mysqli_errno($koneksi) .
            " - " . mysqli_error($koneksi));
    }
    // mengambil data dari database
    $data = mysqli_fetch_assoc($result);
    // apabila data tidak ada pada database maka akan dijalankan perintah ini
    if (!count($data)) {
        echo "<script>alert('Data tidak ditemukan pada database');window.location='index.php';</script>";
    }
} else {
    // apabila tidak ada data GET id_dosen pada akan di redirect ke index.php
    echo "<script>alert('Masukkan data id_dosen.');window.location='index.php';</script>";
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>CRUD Produk dengan gambar - Gilacoding</title>
    <style type="text/css">
    * {
        font-family: "Trebuchet MS";
    }

    h1 {
        text-transform: uppercase;
        color: salmon;
    }

    button {
        background-color: salmon;
        color: #fff;
        padding: 10px;
        text-decoration: none;
        font-size: 12px;
        border: 0px;
        margin-top: 20px;
    }

    label {
        margin-top: 10px;
        float: left;
        text-align: left;
        width: 100%;
    }

    input {
        padding: 6px;
        width: 100%;
        box-sizing: border-box;
        background: #f8f8f8;
        border: 2px solid #ccc;
        outline-color: salmon;
    }

    div {
        width: 100%;
        height: auto;
    }

    .base {
        width: 400px;
        height: auto;
        padding: 20px;
        margin-left: auto;
        margin-right: auto;
        background: #ededed;
    }
    </style>
</head>

<body>
    <center>
        <h1>Edit Produk <?php echo $data['nama_dosen']; ?></h1>
        <center>
            <form method="POST" action="proses_edit.php" enctype="multipart/form-data">
                <section class="base">
                    <!-- menampung nilai id_dosen produk yang akan di edit -->
                    <input name="id_dosen" value="<?php echo $data['id_dosen']; ?>" hidden />
                    <div>
                        <label>Nama</label>
                        <input type="text" name="nama_dosen" value="<?php echo $data['nama_dosen']; ?>" autofocus=""
                            required="" />
                    </div>
                    <div>
                        <label>NIP</label>
                        <input type="text" name="nip_dosen" value="<?php echo $data['nip_dosen']; ?>" />
                    </div>
                    <div>
                        <label>Prodi</label>
                        <input type="text" name="prodi" required="" value="<?php echo $data['prodi']; ?>" />
                    </div>
                    <div>
                        <label>Fakultas</label>
                        <input type="text" name="fakultas" required="" value="<?php echo $data['fakultas']; ?>" />
                    </div>
                    <div>
                        <label>Foto</label>
                        <img src="gambar/<?php echo $data['foto_dosen']; ?>"
                            style="width: 120px;float: left;margin-bottom: 5px;">
                        <input type="file" name="foto_dosen" />
                        <i style="float: left;font-size: 11px;color: red">Abaikan jika tidak merubah gambar produk</i>
                    </div>
                    <div>
                        <button type="submit">Simpan Perubahan</button>
                    </div>
                </section>
            </form>
</body>

</html>